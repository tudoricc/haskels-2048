Nume:Vasile Tudor
Grupa : 321 CC
Dificultate tema:Medie -> Grea
Modificarile facute de mine sunt vizibile in fisierul Board.hs
pentru a juca ,incarca fisierul Interactive.hs si apeleaza functia solitary
Apesi directia si ENTER
Rezolvare:

Am reprezentat tabla de joc ca fiind alcatuita din 2 campuri : O matrice de 
intregi si un scor[reprezentat de un numar intreg]

Show :
Am facut aceasta functie plecand de la cea gasita pe stackoverflow [1]:
Am pus fiecare termen si intre termeni am pus  "|" iar dupa matrice am
afisat "Score:" urmat de scor

build:
Functia primeste o matrice si un numar intreg si pentru rezultat am apelat 
constructorul cu cei 2 parametrii primiti de functie

rows:
Primeste ca parametru un Board,pe care l-am vazut drept constructorul cu cei 2 
parametrii si am returnat primul parametru[matricea]

score:
Primeste ca parametru un Board,pe care l-am vazut drept constructorul cu cei 2 
parametrii si am returnat cel de-al doilea parametru[scorul]

placeRandomCell:
La aceasta functie am creeat cateva functii aditionale,toate aceste functii 
facand acelasi lucru,inlocuiesc intr-un sir o valoare de pe o pozitie cu alta 
valoare[am adaptat pentru cazul meu particular]

Am generat doua numere x si y ,pentru coloana respectiv linie si am vazut daca 
in matricea jocului pe pozitia noastra x si y avem o valoarea mai mare ca 0,daca
era mai mare ca zero insemna ca aveam ceva acolo deci nu puteam pune ceva si
apelam recursiv functia,dar de data aceasta cu alt generator.Daca pe pozitia 
x si y din matrice era 0 atunci schimbam matricea cu ajutorul functiilor 
ajutatoare punand unde trebuie o valoare((x%2 + 1 ) *2) acolo

initialize:
Aceasta functie am creato apeland de 2 ori functia randomCell ,dar de data 
aceasta pe o matrice plina cu zerouri.

moveLeft: -> moveRight: -> moveUp: -> moveDown:
Cele 4 mutari le-am creeat conform enuntului in functie de o singura miscare,
dupa sfatul din enunt am ales miscarea stanga ca fiind cea de baza si cea pe 
care am implementat-o exclusiv restul fiind realizate prin rotatii ale matricei

Am facut functiile de rotare a unei matrice dupa aces link.In functie de fiecare
caz.

Am creeat o functie care ia o linie si verifica fiecare caz potential "periculos"
pentru o linie,primele doua elemente egale ,ultimele doua,primele trei si asa mai
departe,functie care returneaza rezultatul in urma miscarii la stanga pe aceea 
linie.Functia despre care vorbesc se numeste uniteLine si merge impreuna cu 
functia uniteLineScore.

In functia moveLeft am "mapat" miscarea pe matrice[astfel aplicand pentru 
fiecare linie] ,astfel creeand matricea in urma miscarii, iar rezultatul l-am 
obtinut adunand la rezultatul anterior modificarii , scorul rezultat dupa 
modificare[prin funcita uniteLineScore]

PS:Imi cer scuze pentru modul cum arata functia uniteLine ,dar in timp ce testam
fiecare miscare observam cazuri gresite si de aceea a ajuns sa aiba o forma asa 
de urata


isWon:
Am luat matricea si am verificat daca in ea exista un element egal cu 2048 , 
daca da returnam true , altfel apelam recursiv functia pana gaseam.

isLost:
Verificam daca exista un zero in matrice sau daca exista valori alaturate 
atat pentru matricea jocului cand si pentru matricea transpusa pentru a acoperi
cazul in care exista si vecini pe verticala nu numai pe orizontala


Linkuri ajutatoare:
[1]:http://stackoverflow.com/questions/7656948/matrix-constructor-and-method-in-haskell
:- M-a ajutat sa afisez Board-ul[La functia Show]
[2]http://stackoverflow.com/questions/14686731/rotate-char-matrix-image-in-haskell
:- M-a ajutat sa fac rotatie dreapta si stanga pe o matrice