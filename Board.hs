{-
    Tabla de joc și mutările posibile.

    Modulul exportă numai funcțiile enumerate mai jos, ceea ce înseamnă
    că doar acestea vor fi vizibile din alte module. Justificarea vizează
    prevenirea accesului extern la structura obiectelor 'Board', și a eventualei
    coruperi a consistenței interne a acestora.
-}
module Board
    ( Board
    , build
    , rows
    , score
    , initialize
    , placeRandomCell
    , moveUp
    , moveDown
    , moveLeft
    , moveRight
    , isWon
    , isLost
    ) where

import System.Random
import Data.List
{-
    *** TODO ***

    Definiți tipul 'Board', astfel încât să rețină informație despre tabla
    de joc și despre scorul curent.

    Observați că viitorii constructori de date și eventualele câmpuri pe care
    le veți specifica nu vor apărea în lista de funcții exportate de modul
    (vezi explicația de la începutul fișierului).
-}

type Cell = Maybe Int
type Rows = [Cell]
type Table = [Rows]
data Board = Board 
    { _rows :: [[Int]]
    , _scor :: Int
    } deriving Eq

instance Show Board where
   show (Board rows scor) = intercalate "\n" $ map (intercalate " | " . map show ) rows ++ ["Score: "++show scor]
{-
    *** TODO ***

    Instanțiați clasa 'Show' cu tipul 'Board'. Exemplu de reprezentare:

       . |    4 |    2 |    4
       2 |    . |    4 |   32
       . |    . |    8 |  128
       8 |   32 |   64 |    8
    Score: 1216
-}
{-
    *** TODO ***

    Construiește o tablă de joc pe baza unei configurații, furnizate pe linii,
    și a unui scor.
-}
build :: [[Int]] -> Int -> Board
build rowsa scorea =  Board rowsa scorea 


{-
    *** TODO ***

    Întoarce configurația tablei de joc.
-}

rows :: Board -> [[Int]]
rows (Board matrice scor) = matrice 
{-
    *** TODO ***

    Întoarce scorul curent al tablei de joc.
-}
score :: Board -> Int
score (Board matrice scor) = scor

{-
    *** TODO ***

    Plasează aleator o nouă celulă pe tabla de joc.

    Aveți grijă să nu modificați celulele existente!
-}
matrix = [[1,2,3],[2,3]]
lista = [1..]

replaceList :: [a] -> [[a]] -> [[a]]
replaceList _ [] = []
replaceList x (y:ys) = x:ys

replaceHead :: a -> [a] -> [a]
replaceHead _ [] = []
replaceHead x (_:xs) = x:xs

replaceAt :: Int -> a -> [a] -> [a]
replaceAt i x xs = let (prev, remain) = splitAt i xs
                   in prev ++ replaceHead x remain

replaceLine :: [a]->Int->[[a]]->[[a]]
replaceLine final x matrice = let (prev,remain) = splitAt x matrice
                              in prev ++ replaceList final remain

placeRandomCell :: RandomGen g => Board -> g -> (Board, g)
placeRandomCell (Board tabla scor) g = let (x,newg)=randomR (0,3) g --linia--
                                           (y,newG)=randomR (0,3) newg --coloana--
                                       in if tabla !! x !! y /= 0 then (placeRandomCell  (Board tabla scor) newG) 
                                          else ( ( Board (replaceLine (replaceAt y (((mod y 2) +1 )*2) (tabla!!x )) x tabla) scor) , newG)

{-
    *** TODO ***

    Generează aleator o tablă de joc cu două celule ocupate.
-}
possibleTiles = [2,4]
zeroes = (build [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]] 0 )

initialize :: RandomGen g => g -> (Board, g)
initialize  g = let tablamea = fst (placeRandomCell zeroes g)
                in ((fst (placeRandomCell  tablamea g )) ,g)
--initialize = undefined
{-
    *** TODO ***

    Cele patru mutări posibile: stânga, dreapta, sus, jos.
    
    Acestea sunt singurele funcții care modifică scorul.
-}

--Sus este : rotat spre dreapta-> Transpus
--Jos este :

mat:: [[Int]]
mat = [ [ 4, 0, 0, 0 ], [ 4, 2, 0, 0 ] , [ 4, 4, 0, 0 ], [ 4, 8, 0, 0 ] ]
a:: [[Int]]
a= [[1,2,3],[4,5,6],[7,8,9]]
listazero::[Int]
listazero = [0,0,0]
listazero2::[Int]
listazero2=[0,0]



rotateRight :: [[Int]] ->[[Int]]
rotateRight matrice = (map reverse.transpose) matrice

rotateLeft ::[[Int]]->[[Int]]
rotateLeft matrice = reverse ( transpose matrice)

uniteLineScore :: [Int] -> Int
uniteLineScore (x:y:z:t:xs) = if ( (x==y) && (z==t) &&(x/=z)) then (x+y+z+t)
                           else if ((x==y) &&(z==t) && (x==z)) then (x+y+z+t)
                           else  if ((x == y) || ((x==y) && (z==y))) then (x+y)
                           else if ((y == z) || ((y==z)&&(z==t))) then (y+z)
                           else if (z == t) then (z+t)
                           else 0

uniteLine :: [Int] -> [Int]
uniteLine (x:y:z:t:xs) = if ((x==y)&&(y==z) && (z==t) && (x/=0) && (z/=0) && (y/=0) && (t/=0)) then ((x+y):(z+t):listazero2)
                         else if ( (x==y)&&(z==t) && (y/=z)&& (x/=0) && (z/=0) && (y/=0) && (t/=0)) then ((x+y):(z+t):listazero2)
                         else if ((x == y)) && ( x/=0) &&(y/=0) then ((x+y):z:t:[0])
                         else if ((x==y) && (z==y)) &&((x/=0) &&(y/=0) &&(z/=0))  then ((x+y):z:t:[0])
                         else if ((y==z) && (z/=t)&&(t/=0)&&(y/=0)&&(x==0)) then ((y+z):t:listazero2)
                         else if ((x==0)&&(y==z)&&(z==t) && (y/=0)) then ((y+z):t:listazero2)
                         else if ((y == z) &&(y/=0) && (z/=0)) then (x:(y+z):t:[0])
                         else if ((y == z) &&(y/=0) && (z/=0)&&(x/=0)) then (x:(y+z):t:[0])
                         else if ((y==z)&&(z==t)  &&(y/=0) &&(z/=0)&&(t/=0)) then (x:(y+z):t:[0])
                         else if ((x==y) &&(z==t)&&(x/=0) &&(z/=0)) then ((x+y):(z+t):listazero2)
                         else if ((x==y) && (y==0) && ( z==t)&&(t/=0)) then ((z+t):listazero) 
                         else if (z == t)&&((x/=0) &&(y/=0) &&(z/=0)&&(t/=0)) then (x:y:(z+t):[0])
                         else if ((y==0) && (x==0) &&(z/=0) && (t/=0)) then (z:t:listazero2)
                         else if ((y==0) && (x==0) &&(z/=0) && (t==0)) then (z:listazero)
                         else if ((x==0) && (z==0) &&(t==0) && (y/=0)) then (y:listazero)
                         else if ((x==0) && (y==0) && (z==0) && (t/=0)) then (t:listazero)
                         else if ((x==0) &&(y/=0) &&(z/=0)&&(t/=0)) then (y:z:t:[0])
                         else if ((x==y) && (y==0) &&(z/=0)&&(t/=0)) then (z:t:listazero2)
                         else if ((x==0) && (y==0) &&(t/=0) && (z/=0)) then (z:t:listazero2)
                         else (x:y:z:t:xs)

moveLeft :: Board -> Board
moveLeft (Board matrice scor) = (Board ((map uniteLine) matrice) (scor +  (sum ((map uniteLineScore) matrice))  ))

                                
moveUp :: Board -> Board
moveUp (Board matrice scor)= (Board (rotateRight ((map uniteLine) (rotateLeft matrice))) (scor +  (sum ((map uniteLineScore) (rotateLeft matrice)))  ))

moveRight :: Board -> Board
moveRight (Board matrice scor)= (Board (rotateRight(rotateRight ((map uniteLine) (rotateLeft(rotateLeft matrice))))) (scor +  (sum ((map uniteLineScore) (rotateLeft (rotateLeft matrice)))  )))

moveDown :: Board -> Board
moveDown (Board matrice scor)= (Board (rotateLeft ((map uniteLine) (rotateRight matrice))) (scor +  (sum ((map uniteLineScore) (rotateRight matrice)))  ))

{-
    *** TODO ***

    Întoarce 'True' dacă tabla conține o configurație câștigătoare,
    i.e. există cel puțin o celulă cu 2048.
-}
isWon :: Board -> Bool
isWon (Board matrice scor) = if (null matrice) then False else ( 2048 `elem` (head matrice)) || (isWon (Board (tail matrice) scor))


{-
    *** TODO ***

    Întoarce 'True' dacă tabla conține o configurație în care jucătorul pierde,
    i.e. nu există nicio celulă liberă, și nici nu există celule vecine egale,
    pe orizontală sau verticală.
-}
isLostUtil :: Board ->Bool
isLostUtil (Board matrice scor) = if (null matrice) then True 
                              else if (0 `elem` (head matrice)) then False 
                              else if ((head matrice)!!0 == (head matrice)!!1) then False
                              else if ((head matrice)!!1 == (head matrice)!!2) then False
                              else if ((head matrice)!!2 == (head matrice)!!3) then False
                              else isLostUtil (Board (tail matrice) scor)
isLost :: Board -> Bool
isLost (Board matrice scor) = isLostUtil(Board matrice scor) && isLostUtil(Board (transpose matrice) scor)